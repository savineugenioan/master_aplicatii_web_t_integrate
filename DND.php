<?php include("head.php"); ?>
<script>
    function pozapisica(src){
        let prefix='http://localhost/Proiect_diverse/poze_pisici/';
        switch(src){
            case prefix+'pisica.jpg': return "poze_pisici/pisica1.jpg"; break;
            case prefix+'pisica1.jpg': return "poze_pisici/pisica2.jpg"; break;
            case prefix+'pisica2.jpg': return "poze_pisici/pisica3.jpg"; break;
        }
        
    }
    function allowDrop(ev){    
    //if(ev.currentTarget.children.length ==0 && ev.currentTarget.src)
    ev.preventDefault(); 
    }
    function drag(ev)
    {ev.dataTransfer.setData("Text",ev.target.id);}
    function drop(ev)
    {        
        var data=ev.dataTransfer.getData("Text");
        var elem = document.getElementById(data);
        if(ev.currentTarget.children.length >0 && ev.currentTarget.children[0].src == elem.src){
            console.log(elem.src);
            ev.currentTarget.children[0].src = pozapisica(elem.src);
            elem.parentNode.removeChild(elem);
            return;
        }
        else if(ev.currentTarget.children.length >0){return;}
            
        ev.preventDefault();
        ev.currentTarget.appendChild(elem);
    }
</script>
<svg width="510px" height="50px" style="margin:auto;display: block;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">
    <path id="path">
        <animate attributeName="d" id="a1" from="m510,20 h0" to="m10,20 h310" dur="4s" begin="0s;a2.end-1.2" fill="freeze" />
        <animate attributeName="d" id="a2" from="m10,20 h310" to="m-510,20 h310" dur="4s" begin="a1.end" fill="freeze" />
    </path>
    <text  font-size="26" color="black" font-family="Montserrat" fill="chartreuse">
        <textPath  xlink:href="#path">Creati perechi cu Drag&Drop</textPath>
    </text>
</svg>
<div class="flex-container" style="width:510px;margin:auto;">
    <div id="div1drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)">
        <img width="100%" height="100%" src="poze_pisici/pisica.jpg" draggable="true" ondragstart="drag(event)" id="1" ></div>
    <div id="div2drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)">
        <img width="100%" height="100%" src="poze_pisici/pisica.jpg" draggable="true" ondragstart="drag(event)" id="2" ></div>
    <div id="div3drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)">
    <img width="100%" height="100%" src="poze_pisici/pisica.jpg" draggable="true" ondragstart="drag(event)" id="3" ></div>
    <div id="div4drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)">
    <img width="100%" height="100%" src="poze_pisici/pisica.jpg" draggable="true" ondragstart="drag(event)" id="4" ></div>
    <div id="div5drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)">
    <img width="100%" height="100%" src="poze_pisici/pisica.jpg" draggable="true" ondragstart="drag(event)" id="5" ></div>
    <div id="div6drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)">
    <img width="100%" height="100%" src="poze_pisici/pisica.jpg" draggable="true" ondragstart="drag(event)" id="6" ></div>
    <div id="div7drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)">
    <img width="100%" height="100%" src="poze_pisici/pisica.jpg" draggable="true" ondragstart="drag(event)" id="7" ></div>
    <div id="div8drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)">
    <img width="100%" height="100%" src="poze_pisici/pisica.jpg" draggable="true" ondragstart="drag(event)" id="8" ></div>
    <div id="div9drop" class="divdrop" ondrop="drop(event)" ondragover="allowDrop(event)"></div>

</div>

<?php include("footer.php"); ?>