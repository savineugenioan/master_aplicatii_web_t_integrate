window.onload = function() {
	// preluam fereastra afisata in iframe
	var receiver = document.getElementById('receiver1').contentWindow;
  
	// preluam butonul de trimitere a mesajului
	var btn = document.getElementById('send');

	// functia care trimite mesajul 
	function sendMessage(e) {
		// incercam sa prevenim orice comportament implicit al browserului
		e.preventDefault();

		// trimitem un mesaj despre Ana catre fereastra de primire
		receiver.postMessage('1', '*');
	}

	function receiveMessage(e) {

		console.log(e.data);
		let btn1 = document.createElement("button");
		btn1.innerHTML = "Buton "+e.data.message;
		btn1.classList = "btn btn-warning";
		btn1.id = e.data.message;
		btn1.style.marginLeft='5px';
		btn1.style.marginRight='5px';
		if(!document.getElementById(btn1.id))
			btn.parentNode.appendChild(btn1);
		let nr_pag = (Number(e.data.message));
		console.log(nr_pag);
		btn1.addEventListener('click', function(){
			let receiver1 =  document.getElementById('receiver'+nr_pag).contentWindow;
			receiver1.postMessage(nr_pag,
		'http://localhost/Proiect_diverse/subpagini/subpagina'+e.data.message+".html")
		});
	}

	// adaugam un event listener care va asigura executia functiei sendMessage() la apasarea butonului
	btn.addEventListener('click', sendMessage);
	window.addEventListener('message', receiveMessage);
}
