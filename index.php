<?php include("head.php"); ?>
<div class="container1">
    </br>
    <h6 class="lead">Proiect APLICAȚII WEB ȘI TEHNOLOGII INTEGRATE</h6>
    <hr class="my-4">
    <p>SVG&Canvas - exemplificarea unor animatii svg coordonate inre ele si un mini-joculet facut din canvas</p>
    <p>Message - multiple componente iframe care comunica ( primesc si trimit mesaje ) cu o pagina-parinte</p>
    <p>Drag&Drop - un joculet de potrivire a imaginilor facut prin elemente Drag&Drop </p>
    <p>Maps - exemplificare a API-ului GoogleMaps( fara API key )</p>
    <h4>Proiectul mai contine :</h4>
    <p>Instafile - o extensie facuta pentru chromium ce identifica pozele si video-urile de pe instagram si faciliteaza dowloadarea acestora</p>
    <p>OPV - o extensie facuta pentru chromium ce identifica toate sursele video de pe pagina -> foarte folositoare pentru siteurile de filme cu multe reclame &#128513</p>
  </div>
<?php include("footer.php"); ?>
