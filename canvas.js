
var DAMPING = 0.999;
var DAMPING1 = 1;

function Particle(x, y) {
this.x = this.oldX = x;
this.y = this.oldY = y;
}

Particle.prototype.integrate = function() {
var velocityX = (this.x - this.oldX) * DAMPING ;
var velocityY = (this.y - this.oldY) * DAMPING ;
this.oldX = this.x;
this.oldY = this.y;
this.x += velocityX;
this.y += velocityY;
};

Particle.prototype.attract = function(x, y) {
var dx = x - this.x;
var dy = y - this.y;
var distance = Math.sqrt(dx * dx + dy * dy);//  * DAMPING1 ;
//DAMPING1 *=1.00000001; // incetinire 
//getRndInteger(100,3000);//cat de departe merg de punctul de atractie si viteza
this.x += dx / distance;
this.y += dy / distance;
};

Particle.prototype.draw = function() {
ctx.strokeStyle = "white"//random_rgba();;
ctx.lineWidth = 1;
ctx.beginPath();
ctx.moveTo(this.oldX, this.oldY);
ctx.lineTo(this.x, this.y);
ctx.stroke();
};

var display = document.getElementById('display');
var ctx = display.getContext('2d');
var particles = [];
var width = display.width = window.innerWidth;
var height = display.height = window.innerHeight;
var mouse = { x: width * 0.5, y: height * 0.5 };

for (var i = 0; i < 1000; i++) {
particles[i] = new Particle(Math.random() * width, Math.random() * height);
}

// display.addEventListener('mousemove', onMousemove);

// function onMousemove(e) {
// mouse.x = e.clientX;
// mouse.y = e.clientY;
// }

requestAnimationFrame(frame);

async function frame() {
    requestAnimationFrame(frame);
    ctx.clearRect(0, 0, width, height);//panza paianjen :)
    for (var i = 0; i < particles.length; i++) {
        particles[i].attract(getRndInteger(-300,1800), getRndInteger(-300,1800));
        particles[i].integrate();
        particles[i].draw();
    }
}

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
}
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

function delay(milliseconds){
    return new Promise(resolve => {
        setTimeout(resolve, milliseconds);
    });
}