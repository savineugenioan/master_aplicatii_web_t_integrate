<?php include("head.php"); ?>

<script type="text/javascript">
        var culori_pisici=[0,1,2,3];
        var fill_color=["white","gray","orange","wheat"];
            var stroke_collor=["green","red","black",'blue'];
        function desenare_pisica(nr_id)
        { 
            for(var counter = 0; counter <= nr_id; counter++){
                var canvas = document.getElementById("canvas_pisica"+counter);
                var context = canvas.getContext('2d');
                var centerX = canvas.width / 2;
                var centerY = canvas.height / 2;
                var radius = 40;
                if(culori_pisici[counter]==3) culori_pisici[counter]=0;
                else culori_pisici[counter]+=1;
                context.fillStyle = fill_color[culori_pisici[counter]];
                context.strokeStyle =stroke_collor[culori_pisici[counter]];
                //console.log('id: '+counter+' '+fill_color[culori_pisici[counter]]+' '+stroke_collor[culori_pisici[counter]])

                context.lineWidth = 5;

                //urechi
                context.beginPath();
                context.lineTo(55, 40);
                context.lineTo(55, 10);
                context.lineTo(75, 30);
                context.lineTo(95, 10);
                context.lineTo(95, 40);
                context.fill();
                context.stroke();

                //cap
                context.beginPath();
                context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
                context.fill();
                context.stroke();

                context.beginPath();
                context.arc(55, 60, 2, 0, 2 * Math.PI, false);
                context.stroke();
                context.beginPath();
                context.arc(95, 60, 2, 0, 2 * Math.PI, false);
                context.stroke();

                //gura
                context.beginPath();
                context.arc(75, 90, 5, 0,  Math.PI, false);
                context.stroke();
            }

        }
    </script>
<svg width="100%" height="1000px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">
    <script type="text/JavaScript">
    <![CDATA[ function showColor() { alert("Culoarea dreptunghiului este: "+
    document.getElementById("rect1").getAttributeNS(null,"fill")+" ... nu vedeai si singur?! :))"); } 
    function showArea(event){ 
        var width =parseFloat(event.target.getAttributeNS(null,"width")); 
        var height = parseFloat(event.target.getAttributeNS(null,"height")); 
        alert("Area of the rectangle is: " +width +"x"+ height); 
    } ]]>
    </script>
    <circle cx="40" cy="40" r="30" stroke="yellow" stroke-width="10">
        <animate attributeName="cx" id="a11" values="99;599;" begin="0;a44.end" dur="1s"  fill="freeze" />
        <animate attributeName="cx" id="a22" values="599;799;" begin="a11.end" dur="1s"  fill="freeze" />
        <animate attributeName="cx" id="a33" values="799;599;" begin="a22.end" dur="1s"  fill="freeze" />
        <animate attributeName="cx" id="a44" values="599;99" begin="a33.end" dur="1s"  fill="freeze" />
        <animate attributeName="fill" begin="0s" dur="4s" values="red; blue" repeatCount="indefinite" />
        <animate attributeName="stroke" begin="0s" dur="4s" values="yellow; orange" repeatCount="indefinite" />
    </circle>
    <path id="path">
        <animate attributeName="d" from="m50,110 h0" to="m200,110 h1100" dur="6s" begin="0s"
            repeatCount="indefinite" />
    </path>
    <text  font-size="26" color="black" font-family="Montserrat" fill="yellow">
        <textPath  xlink:href="#path">Exemplificare canvas si SVG.</textPath>
    </text>
    <defs>
        <pattern id="pattern1" patternUnits="userSpaceOnUse" x="0" y="0" width="100" height="100"
            viewBox="-1 0 5 4">
            <path d="M 0.5 2 
        C 0.5 2 1.5 -0.5 2.5 2
        C 2.5 2 1.5 4.5 0.5 2
        z" fill="yellow" stroke="purple" />
        </pattern>
        <radialGradient id="gl1">
            <stop offset="5%" stop-color="##8c429b" />
            <stop offset="100%" stop-color="#a5c514" />
        </radialGradient>
    </defs>

    
    <rect x="50" y="150" width="100" height="100" stroke="orange" stroke-width="3" onClick="showArea(event)">
        <animate attributeName="x" id="a1" values="50;550" begin="0s;a4.end" dur="1s" fill="freeze" />
        <animateMotion id="a2" path="M0,0 q60,-50 100,0 q60, -50 100,0" begin="a1.end" dur="1s" fill="freeze" />
        <animateMotion id="a3" path="M200,0 q-60,-50 -100,0 q-60, -50 -100,0" begin="a2.end" dur="1s" fill="freeze" />
        <animate attributeName="x" id="a4" values="550;50" begin="a3.end" dur="1s" fill="freeze" />
        <animate attributeName="fill" begin="0s" dur="1s" values="red;yellow;blue" repeatCount="indefinite" />
    </rect>
    <rect x="50" y="260" width="100" height="100" stroke="orange" stroke-width="3" onClick="showArea(event)">
        <animate attributeName="x" id="a1" values="50;550" begin="0s;a4.end" dur="1s" fill="freeze" />
        <animate attributeName="x" id="a2" values="550;750" begin="a1.end" dur="1s" fill="freeze" />
        <animate attributeName="x" id="a3" values="750;550" begin="a2.end" dur="1s" fill="freeze" />
        <animate attributeName="x" id="a4" values="550;50" begin="a3.end" dur="1s" fill="freeze" />
        <animate attributeName="fill" begin="0s" dur="1s" values="blue;red;yellow;" repeatCount="indefinite" />
    </rect>
    <rect x="50" y="370" width="100" height="100" stroke="orange" stroke-width="3" onClick="showArea(event)">
        <animate attributeName="x" id="a1" values="50;550" begin="0s;a4.end" dur="1s" fill="freeze" />
        <animateMotion id="a2" path="M0,0 q60,50 100,0 q60, 50 100,0" begin="a1.end" dur="1s" fill="freeze" />
        <animateMotion id="a3" path="M200,0 q-60,50 -100,0 q-60, 50 -100,0" begin="a2.end" dur="1s" fill="freeze" />
        <animate attributeName="x" id="a4" values="550;50" begin="a3.end" dur="1s" fill="freeze" />
        <animate attributeName="fill" begin="0s" dur="1s" values="yellow;blue;red;" repeatCount="indefinite" />
    </rect>

    <?php
    $x=50;$y=200;
    for($i = 0; $i < 2; $i++){
        for($j = 0; $j < 4; $j++){
            echo'
            <rect x="'.(50+4*$x*$j).'" y="'.(550+$y*$i).'" width="200" height="200" stroke="orange" fill="url(#pattern1)" id="rect1" >
            <set attributeName ="stroke-width" id="set1" to="2" begin="0s;set2.end" dur="1s" fill="freeze" />
            <set attributeName ="stroke-width" id="set2" to="0" begin="set1.end" dur="1s" fill="freeze" />
            </rect>';
    }} ?>
</svg>

<div class="flex-container" style="width:680px;margin-left:110px;">
<button class = "button_scale btn btn-warning" onclick="desenare_pisica(3)">Amesteca culori </button>
<button class = "button_scale btn btn-warning" onclick="desenare_pisica(1)">Amesteca culori </button>
<button class = "button_scale btn btn-warning" onclick="desenare_pisica(0)">Amesteca culori </button>
<button class = "button_scale btn btn-warning" onclick="desenare_pisica(2)">Amesteca culori </button>
<canvas class = "divdrop " id="canvas_pisica0" width="150" height="150" ></canvas>
<canvas class = "divdrop " id="canvas_pisica1" width="150" height="150" ></canvas>
<canvas class = "divdrop " id="canvas_pisica2" width="150" height="150" ></canvas>
<canvas class = "divdrop " id="canvas_pisica3" width="150" height="150" ></canvas>
</div>
<script>desenare_pisica(3);</script>

<?php include("footer.php"); ?>