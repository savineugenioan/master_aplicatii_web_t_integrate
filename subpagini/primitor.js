window.onload = function() {
	var messageEle = document.getElementById('message');

	function receiveMessage(e) {
		if (e.origin !== "http://localhost")
			return;

		window.parent.postMessage({message: (Number(e.data)+1)});
		messageEle.innerHTML = "<br>" + e.data;
	}

	window.addEventListener('message', receiveMessage);
}
